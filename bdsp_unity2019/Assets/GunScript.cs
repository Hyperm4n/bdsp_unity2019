﻿using UnityEngine;
using System.Collections;

public class GunScript : MonoBehaviour
{
    public int damage = 10;
    public float range = 100f;
    private Ray shootRay;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            shootRay.origin = transform.position;
            shootRay.direction = transform.forward;
            RaycastHit shootHit;
            if (Physics.Raycast(shootRay, out shootHit, range))
            {
                if (shootHit.transform.tag == "Goblin")
                    Debug.Log("tir");
                shootHit.transform.GetComponent<EnemyScript>().life -= damage;
            }
        }
    }
}
